package com.isu.service.onboarding.userType;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.isu.service.R;
import com.isu.service.onboarding.configuration.OnBoardSharedPreference;
import com.isu.service.onboarding.configuration.OnboardConfig;
import com.isu.service.onboarding.configuration.Validate;
import com.isu.service.onboarding.otpverification.UserOtpActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class NewUserActivity extends AppCompatActivity {
    private static final String TAG = NewUserActivity.class.getSimpleName();
    TextInputEditText userNameET, passwordET, confirmPasswordET;
    TextInputLayout userNameLayout, passwordLayout, confirmLayout;
    ProgressBar userCheck;
    ImageView check;
    String userName, password;

    List<String> userNameFS; //temporary user names present in firestore collection

    boolean isUser = false, isPassword = false, isConfirm = false; //edit text validations

    Button submit;

    OnBoardSharedPreference preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_user);
        OnboardConfig.whiteStatusNav(this);

        preference = new OnBoardSharedPreference(this);

        getFrestoreCollection(); //fetch usernames from firestore's temporary username list

        userNameET = findViewById(R.id.new_user_name);
        passwordET = findViewById(R.id.new_user_password);
        confirmPasswordET = findViewById(R.id.new_user_password_confirm);
        userNameLayout = findViewById(R.id.new_user_name_layout);
        passwordLayout = findViewById(R.id.new_user_password_layout);
        confirmLayout = findViewById(R.id.new_user_password_confirm_layout);
        userCheck = findViewById(R.id.user_check);
        submit = findViewById(R.id.new_user_submit);
        check = findViewById(R.id.user_check_image);

        check.setVisibility(View.GONE);
        userCheck.setVisibility(View.GONE);

        userNameET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String name = userNameET.getText().toString();
                check.setVisibility(View.GONE);
                if (!name.equals("")) {
                    if (name.length() >= 4) {
                        if (name.contains(" ")) {
                            userNameLayout.setError("User name must not contain any spaces.");
                            isUser = false;
                        } else {
                            userNameLayout.setError(null);
                            checkUser(name); //check username availability with elastic search
                        }
                    } else {
                        userNameLayout.setError("User name must be at least 4 characters.");
                        isUser = false;
                    }
                } else {
                    userNameLayout.setError("User name must be at least 4 characters.");
                    isUser = false;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        passwordET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                if (s.length() < 6) {
                    passwordLayout.setError("The password must be at least 6 characters.");
                    isPassword = false;
                    isConfirm = false;
                } else {
                    if (!Validate.isPassword(passwordET.getText().toString())) {
                        passwordLayout.setError(getResources().getString(R.string.changePasswordText));
                        isPassword = false;
                        isConfirm = false;
                    } else {
                        passwordLayout.setError(null);
                        isPassword = true;
                    }

                    String con = confirmPasswordET.getText().toString();
                    String p = passwordET.getText().toString();
                    if (!con.equals("") && !con.equals(p)) {
                        confirmLayout.setError("Password & Confirm Password must be same.");
                        isConfirm = false;
                    } else if (!con.equals("") && con.equals(p)) {
                        confirmLayout.setError(null);
                        isConfirm = true;
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        confirmPasswordET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String p = passwordET.getText().toString();
                String conP = confirmPasswordET.getText().toString();
                if (p.equals("")) {
                    confirmLayout.setError("Enter password first.");
                    isConfirm = false;
                } else if (!p.equals(conP)) {
                    confirmLayout.setError("Password & Confirm Password must be same.");
                    isConfirm = false;
                } else {
                    confirmLayout.setError(null);
                    if (isPassword) {
                        isConfirm = true;
                    } else {
                        isConfirm = false;
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isUser && isPassword && isConfirm) {
                    userName = userNameET.getText().toString();
                    if (userName.equals("")) {
                        userNameLayout.setError("User name must be at least 4 characters.");
                        isUser = false;
                        check.setImageDrawable(getResources().getDrawable(R.drawable.ic_user_wrong));
                        Validate.showAlert(NewUserActivity.this, "Enter a valid user name.");
                    } else { // passes all the validations
                        password = passwordET.getText().toString();
                        preference.setStringValue(OnboardConfig.USER_NAME_KEY, userName);
                        preference.setStringValue(OnboardConfig.USER_PASSWORD_KEY, password);
                        Intent intent = new Intent(NewUserActivity.this, UserOtpActivity.class);
                        startActivity(intent);
//                    intent.putExtra("USER_TYPE", "new");
                    }
                } else if (!isUser) {
                    Validate.showAlert(NewUserActivity.this, "Enter a valid user name.");
                } else if (!isPassword) {
                    Validate.showAlert(NewUserActivity.this, "Enter a valid password.");
                } else {
                    Validate.showAlert(NewUserActivity.this, "Password and confirm password must match.");
                }
            }
        });
    }

    private void getFrestoreCollection() {
        userNameFS = new ArrayList<>();
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        CollectionReference uNames = db.collection("SelfUserNames");

        uNames.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot snapshots, @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.e(TAG, "onEvent: error occured");
                }

                if (snapshots.isEmpty()) {
                    Log.e(TAG, "onEvent: empty snap");
                } else {
                    List<DocumentSnapshot> doc = snapshots.getDocuments();
                    String name = userNameET.getText().toString();
                    userNameFS = new ArrayList<>();
                    for (int i = 0; i < doc.size(); i++) {
                        userNameFS.add(doc.get(i).getId());
                    }

                    if (name.length() != 0) {
                        userCheck.setVisibility(View.VISIBLE);
                        if (userNameFS.contains(name)) {
                            userCheck.setVisibility(View.GONE);
                            userNameLayout.setError("This userName already taken");
                            check.setVisibility(View.VISIBLE);
                            check.setImageDrawable(getResources().getDrawable(R.drawable.ic_user_wrong));
                            isUser = false;
                        } else {
                            userCheck.setVisibility(View.GONE);
                            userNameLayout.setError(null);
                            check.setVisibility(View.VISIBLE);
                            check.setImageDrawable(getResources().getDrawable(R.drawable.ic_user_correct));
                            isUser = true;
                        }
                    }


                }


            }
        });

    }

    private void checkUser(String p) {
        userCheck.setVisibility(View.VISIBLE);
        JSONObject obj = new JSONObject();

        try {
            obj.put("user_name", p);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(OnboardConfig.getElasticUser())
                .setPriority(Priority.HIGH)
                .addJSONObjectBody(obj)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e(TAG, "onResponse: " + response);//user name is available
                        if (userNameFS.contains(p)) { //checks if username is present in firestore's temporary usernames
                            userCheck.setVisibility(View.GONE);
                            userNameLayout.setError("This userName already taken");
                            check.setVisibility(View.VISIBLE);
                            check.setImageDrawable(getResources().getDrawable(R.drawable.ic_user_wrong));
                            isUser = false;
                        } else {
                            userCheck.setVisibility(View.GONE);
                            userNameLayout.setError(null);
                            check.setVisibility(View.VISIBLE);
                            check.setImageDrawable(getResources().getDrawable(R.drawable.ic_user_correct));
                            isUser = true;
                        }
                    }

                    @Override
                    public void onError(ANError anError) {//user name is not available
                        Log.e(TAG, "onError: " + anError.getErrorBody());
                        Log.e(TAG, "onError: " + anError.getErrorDetail());
                        userCheck.setVisibility(View.GONE);
                        userNameLayout.setError("This userName already taken");
                        check.setVisibility(View.VISIBLE);
                        check.setImageDrawable(getResources().getDrawable(R.drawable.ic_user_wrong));
                        isUser = false;
                    }
                });
    }

}