package com.isu.service.onboarding.digilocker;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.isu.service.R;
import com.isu.service.onboarding.configuration.OnboardConfig;


public class DigiWebActivity extends AppCompatActivity {
    WebView webView;
    ProgressBar proressV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_digi_web);
        OnboardConfig.whiteStatusNav(this);

        Toolbar toolbar = findViewById(R.id.digi_web_toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_left);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        webView = findViewById(R.id.digi_web);
        proressV = findViewById(R.id.digi_progress);

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                proressV.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                proressV.setVisibility(View.GONE);
                super.onPageFinished(view, url);
            }


        });
        webView.loadUrl("https://accounts.digitallocker.gov.in/signin");
        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        webView.cancelLongPress();
    }

    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack();
        } else {
            super.onBackPressed();
        }
    }

}
