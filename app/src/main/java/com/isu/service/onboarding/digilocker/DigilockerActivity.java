package com.isu.service.onboarding.digilocker;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.isu.service.R;
import com.isu.service.onboarding.configuration.OnBoardSharedPreference;
import com.isu.service.onboarding.configuration.OnboardConfig;
import com.isu.service.onboarding.ekyc.EkycRequestActivity;

import org.json.JSONException;
import org.json.JSONObject;

public class DigilockerActivity extends AppCompatActivity {
    private static final String TAG = DigilockerActivity.class.getSimpleName();
    WebView webView;
    TextView textView;
    String digiText, user;

    boolean complete;

    ProgressDialog dialog;
    OnBoardSharedPreference preference;
    ProgressBar proressV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_digilocker);
        OnboardConfig.whiteStatusNav(this);
        preference = new OnBoardSharedPreference(this);
        proressV = findViewById(R.id.proressV);

        complete = false;
        webView = findViewById(R.id.digilocker_webview);
        textView = findViewById(R.id.digi_text);

        String url = getIntent().getStringExtra("digiUrl");
        user = getIntent().getStringExtra("digiUser");
        digiText = "Username \t\t: " + user;
        if (url.equalsIgnoreCase("refetchDigiLocker")){
            webView.setVisibility(View.INVISIBLE);
            reverifyDigilocker();
        }
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);

                Log.e(TAG, "onPageStarted: loading " + url);
                proressV.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                Log.e(TAG, "onPageFinished: finish " + url);
                proressV.setVisibility(View.GONE);
                super.onPageFinished(view, url);
            }

            @Override
            public void onLoadResource(WebView view, String url) {
                // TODO Auto-generated method stub
                super.onLoadResource(view, url);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.e(TAG, "shouldOverrideUrlLoading: clicked on " + url);
                if (url.contains("digilocker/android")) {
                    webView.setVisibility(View.GONE);
                    Log.e(TAG, "shouldOverrideUrlLoading: now fetch data");
                    textView.setVisibility(View.VISIBLE);
                    complete = true;

                    fetchDigiLocker();


                }
                if (url.contains(OnboardConfig.getDigiLoader())) {
                    proressV.setVisibility(View.VISIBLE);
                }
                if (url.contains("error=access_denied")){
                    webView.setVisibility(View.INVISIBLE);
                    deniedAlert();
                }


                return super.shouldOverrideUrlLoading(view, url);
            }
        });
        webView.loadUrl(url);
        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        webView.cancelLongPress();

    }

    private void reverifyDigilocker() {
        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();

        String name = preference.getStringValue(OnboardConfig.USER_NAME_KEY);
        String url = OnboardConfig.getDigiRefetchUrl() + "code=" + name + "&state=" + name + "&caller=" + name;

        AndroidNetworking.get(url)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e(TAG, "onResponse: " + response);
                        try {
                            String status = response.getString("status");
                            if (status.equals("0")) {
                                String pan = response.getString("PAN");
//                                preference.setStringValue(OnboardConfig.PAN_CARD_KEY, pan);
                                startActivity(new Intent(DigilockerActivity.this, EkycRequestActivity.class));
                                dialog.dismiss();
                                finish();

                            } else {
                                showAlertDialog("Please add your PAN Card to Digi-Locker.");
                                dialog.dismiss();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            dialog.dismiss();
                            showAlertDialog("Please add your PAN Card to Digi-Locker.");
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        try {
                            JSONObject errorObject = new JSONObject(anError.getErrorBody());
                            Log.e(TAG, "onError: " + errorObject);
                            Toast.makeText(DigilockerActivity.this, errorObject.getString("statusDesc"), Toast.LENGTH_LONG).show();
                            showAlertDialog("Please add your PAN Card to Digi-Locker.");
                            dialog.dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            dialog.dismiss();
                            showAlertDialog("Please add your PAN Card to Digi-Locker.");
                        }

                    }
                });


    }

    void fetchDigiLocker() {
        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();

        JSONObject obj = new JSONObject();

        try {
            obj.put("username", user);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(OnboardConfig.getDigiFetchUrl())
                .setPriority(Priority.HIGH)
                .addJSONObjectBody(obj)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e(TAG, "onResponse: " + response);
                        try {
                            String status = response.getString("status");
                            if (status.equals("0")) {
                                JSONObject data = response.getJSONObject("data");
                                String name = data.getString("name");
                                String aadhaar = "";
//                                digiText = digiText+"\nName \t\t: "+name;
//                                if (data.has("ADHAR")){
//                                    digiText = digiText+"\nAadhaar \t\t: "+data.getString("ADHAR");
//                                }
//                                if (data.has("PANCR")){
//                                    digiText = digiText+"\nPancard \t\t: "+data.getString("PANCR");

//                                }
                                textView.setVisibility(View.GONE);
                                if (!data.has("ADHAR")) {
                                    showDigiAlert("Please add your Aadhaar Card to Digi-Locker.");
                                } else {
                                    storeSubStatus();
                                }
                                if (!data.has("PANCR")) {
                                    storeSubStatus();
//                                    Validate.showAlert(DigilockerActivity.this, "Please add your PAN Card to Digi-Locker.");
                                    showAlertDialog("Please add your PAN Card to Digi-Locker.");

                                    String surName = name.split(" ")[name.split(" ").length - 1];
                                    String firstName = name.substring(0, name.length() - surName.length());

                                    if (surName.equals("")) {
                                        surName = "N/A";
                                    }

                                    if (data.has("ADHAR")){
                                        aadhaar = data.getString("ADHAR");
                                    }

                                } else {

                                    String surName = name.split(" ")[name.split(" ").length - 1];
                                    String firstName = name.substring(0, name.length() - surName.length());
                                    if (surName.equals("")) {
                                        surName = "N/A";
                                    }

                                    if (data.has("ADHAR")){
                                        aadhaar = data.getString("ADHAR");
                                    }


//                                    preference.setStringValue(OnboardConfig.PAN_CARD_KEY, data.getString("PANCR"));
//                                    preference.setStringValue(OnboardConfig.AADHAAR_KEY, aadhaar);
//                                    preference.setStringValue(OnboardConfig.FULL_NAME_KEY, name);
//                                    preference.setStringValue(OnboardConfig.FIRST_NAME_KEY, firstName);
//                                    preference.setStringValue(OnboardConfig.LAST_NAME_KEY, surName);

                                    Intent i = new Intent(DigilockerActivity.this, EkycRequestActivity.class);
                                    startActivity(i);
                                    finish();
                                }

                                textView.setText(digiText);
                                dialog.dismiss();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            textView.setText(e.getMessage());
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e(TAG, "onError: body " + anError.getErrorBody());
                        Log.e(TAG, "onError: details " + anError.getErrorDetail());
                        Toast.makeText(DigilockerActivity.this, "Please try again.", Toast.LENGTH_LONG);
                        textView.setText(anError.getErrorDetail());
                        dialog.dismiss();
                    }
                });

    }

    private void storeSubStatus() {
        JSONObject obj = new JSONObject();

        try {
            obj.put("type", "STATUS_UPDATE");
            obj.put("username", preference.getStringValue(OnboardConfig.USER_NAME_KEY));
            obj.put("subStatus", "7");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(OnboardConfig.getSaveUrl())
                .setPriority(Priority.HIGH)
                .addJSONObjectBody(obj)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e(TAG, "onResponse: " + response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e(TAG, "onError: body " + anError.getErrorBody());
                    }
                });
    }

    @Override
    public void onBackPressed() {
        if (complete) {
            finish();
        } else {
            if (webView.canGoBack()) {
                webView.goBack();
            } else {
                super.onBackPressed();
            }
        }
    }

    public void showDigiAlert(String statusDesc) {
        try {
            AlertDialog.Builder alertbuilderupdate;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                alertbuilderupdate = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
            } else {
                alertbuilderupdate = new AlertDialog.Builder(this);
            }
            alertbuilderupdate.setCancelable(false);
            // String message = "Session is already running !!! Please login after sometimes.";
            alertbuilderupdate.setTitle("Alert")
                    .setMessage(statusDesc)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
            AlertDialog alert11 = alertbuilderupdate.create();
            alert11.show();
        } catch (Exception e) {

        }
    }


    private void deniedAlert(){
        AlertDialog.Builder alertbuilderupdate;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            alertbuilderupdate = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            alertbuilderupdate = new AlertDialog.Builder(this);
        }
        alertbuilderupdate.setCancelable(false);
        // String message = "Session is already running !!! Please login after sometimes.";
        alertbuilderupdate.setTitle("Alert")
                .setMessage("Allow Digi-Locker to access your documents.")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        finish();
                    }
                });

        AlertDialog alert11 = alertbuilderupdate.create();
        alert11.show();
    }


    private void showAlertDialog(String statusDesc) {
        AlertDialog.Builder alertbuilderupdate;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            alertbuilderupdate = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            alertbuilderupdate = new AlertDialog.Builder(this);
        }
        alertbuilderupdate.setCancelable(false);
        // String message = "Session is already running !!! Please login after sometimes.";
        alertbuilderupdate.setTitle("Alert")
                .setMessage(statusDesc)
                .setPositiveButton("Upload PAN Card", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(DigilockerActivity.this, DigiPanActivity.class);
                        startActivity(intent);
                        dialog.cancel();
                    }
                });
        alertbuilderupdate.setNegativeButton(
                "Add PAN Card via Digi-Locker",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        startActivity(new Intent(DigilockerActivity.this, FetchDigiActivity.class));
                        finish();
                        dialog.cancel();
                    }
                });
        alertbuilderupdate.setNeutralButton(
                "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        finish();
                    }
                });
        AlertDialog alert11 = alertbuilderupdate.create();
        alert11.show();
    }

}
