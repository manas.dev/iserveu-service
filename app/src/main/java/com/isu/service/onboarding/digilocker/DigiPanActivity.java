package com.isu.service.onboarding.digilocker;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatCheckBox;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.isu.service.R;
import com.isu.service.onboarding.configuration.OnBoardSharedPreference;
import com.isu.service.onboarding.configuration.OnboardConfig;
import com.isu.service.onboarding.configuration.Validate;
import com.isu.service.onboarding.ekyc.EkycRequestActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class DigiPanActivity extends AppCompatActivity {
    private static final String TAG = DigiPanActivity.class.getSimpleName();

    private EditText panET;
    private ImageView panImage, panSelect, imageEdit;
    private Uri filePath;
    private final int PICK_IMAGE_REQUEST = 71;
    private String pan;
    ProgressDialog dialog;
    OnBoardSharedPreference preference;
    String name, imageURL, userName;

    FirebaseStorage storage;
    StorageReference storageReference;

    boolean imageAdded = false;

    Button next, prev;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_digi_pan);

        OnboardConfig.whiteStatusNav(this);
        preference = new OnBoardSharedPreference(this);
        name = preference.getStringValue(OnboardConfig.NAME_KEY);
        userName = preference.getStringValue(OnboardConfig.USER_NAME_KEY);
        storage = FirebaseStorage.getInstance("gs://iserveu_storage");
        storageReference = storage.getReference();

        panSelect = findViewById(R.id.digi_pan_image_select);
        panImage = findViewById(R.id.digi_pan_image);
        imageEdit = findViewById(R.id.digi_pan_image_edit);

        panImage.setVisibility(View.GONE);
        panET = findViewById(R.id.digi_pan_verify);

        next = findViewById(R.id.digi_pan_verify_next);
        prev = findViewById(R.id.digi_pan_verify_prev);

        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Validate.showExitAlert(DigiPanActivity.this);
            }
        });

        panSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseImage();
            }
        });

        imageEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseImage();
            }
        });

        panET.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
            }

            @Override
            public void afterTextChanged(Editable et) {
                String s = et.toString();
                if (!s.equals(s.toUpperCase())) {
                    s = s.toUpperCase();
                    panET.setText(s);
                    panET.setSelection(panET.length());
                }
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pan = panET.getText().toString();
                if (!imageAdded) {
                    Validate.showAlert(DigiPanActivity.this, "Please Upload your PAN Image");
                } else if (pan.equals("")) {
                    Validate.showAlert(DigiPanActivity.this, "Please enter your PAN Card Number");
                } else {
//                    verifyPan(pan);
                    preference.setStringValue(OnboardConfig.PAN_CARD_KEY, pan);
                    submitPan(pan);
                }
//                if (!pan.equals("")) {
//                    verifyPan(pan);
//                    startActivity(new Intent(DigiPanActivity.this, EkycRequestActivity.class));
//                } else {
//                    Toast.makeText(DigiPanActivity.this, "Please enter your PAN Card Number", Toast.LENGTH_LONG).show();
//                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null) {
            filePath = data.getData();
            String mimeType = getContentResolver().getType(filePath);

            Cursor returnCursor =
                    getContentResolver().query(filePath, null, null, null, null);
            /*
             * Get the column indexes of the data in the Cursor,
             * move to the first row in the Cursor, get the data,
             * and display it.
             */
            int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
            int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
            returnCursor.moveToFirst();
            Log.e(TAG, "onActivityResult: cursor " + returnCursor);
            Log.e(TAG, "onActivityResult: name " + returnCursor.getString(nameIndex) + ", " + nameIndex);
            Log.e(TAG, "onActivityResult: size " + returnCursor.getLong(sizeIndex) + ", " + sizeIndex);

            Log.e(TAG, "onActivityResult: check " + (returnCursor.getLong(sizeIndex) < 5000000));


            if (mimeType != null && (mimeType.contains("png") || mimeType.contains("gif") || mimeType.contains("jpg") || mimeType.contains("jpeg"))) {
                if (returnCursor.getLong(sizeIndex) < 5000000) {
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                        panImage.setImageBitmap(bitmap);
                        panImage.setVisibility(View.VISIBLE);
                        imageEdit.setVisibility(View.VISIBLE);
                        panSelect.setVisibility(View.GONE);
                        imageAdded = true;

                        uploadImage();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    Validate.showAlert(DigiPanActivity.this, "Selected Image size must not be bigger than 5MB.");
                }
            } else {
                Validate.showAlert(DigiPanActivity.this, "Your PAN Card image must be in png, jpg, jpeg or gif format.");
            }
        }
    }

    private void uploadImage() {
        if (filePath != null) {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            StorageReference ref = storageReference.child("KYC_NEW/" + userName + "/" + userName + "_panImg.png");
            ref.putFile(filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            progressDialog.dismiss();

                            ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    Log.e(TAG, "onSuccess: uri= " + uri.toString());
                                    imageURL = uri.toString();
                                }
                            });

                            Toast.makeText(DigiPanActivity.this, "Upload successfully", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(DigiPanActivity.this, "Failed " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot
                                    .getTotalByteCount());
                            progressDialog.setMessage("Uploaded " + (int) progress + "%");
//                            Log.e(TAG, "onProgress: progress " + progress);
                        }
                    });

        }
    }

    private void chooseImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    private void submitPan(String number) {
        dialog = new ProgressDialog(DigiPanActivity.this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();

        JSONObject obj = new JSONObject();

        try {
            obj.put("username", preference.getStringValue(OnboardConfig.USER_NAME_KEY));
            obj.put("pan", number);
            obj.put("url", imageURL);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e(TAG, "submitPan: request body " + obj);

        AndroidNetworking.post(OnboardConfig.getDigiPanSaveUrl())
                .setPriority(Priority.HIGH)
                .addJSONObjectBody(obj)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e(TAG, "onResponse: " + response);

                        preference.setStringValue(OnboardConfig.PAN_CARD_KEY, number);

                        startActivity(new Intent(DigiPanActivity.this, EkycRequestActivity.class));
//                        OnboardConfig.setDocument(document);
                        dialog.dismiss();
                        finish();
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e(TAG, "onError: " + anError.getErrorDetail());
                        dialog.dismiss();
                        try {
                            JSONObject errorObject = new JSONObject(anError.getErrorBody());
                            String statusDesc = errorObject.getString("statusDesc");
                            Toast.makeText(DigiPanActivity.this, statusDesc, Toast.LENGTH_LONG).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }
    @Override
    public void onBackPressed() {
        Validate.showExitAlert(this);
    }
}
