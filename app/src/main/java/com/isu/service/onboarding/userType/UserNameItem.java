package com.isu.service.onboarding.userType;

public class UserNameItem {
    String userName, password, adminName, phone;

    public UserNameItem(String userName, String password, String adminName, String phone) {
        this.userName = userName;
        this.password = password;
        this.adminName = adminName;
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public String getAdminName() {
        return adminName;
    }
}
