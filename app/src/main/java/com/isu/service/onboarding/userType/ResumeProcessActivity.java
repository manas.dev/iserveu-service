package com.isu.service.onboarding.userType;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.isu.service.R;
import com.isu.service.onboarding.configuration.OnBoardSharedPreference;
import com.isu.service.onboarding.configuration.OnboardConfig;
import com.isu.service.onboarding.configuration.Validate;
import com.isu.service.onboarding.otpverification.WelcomeActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ResumeProcessActivity extends AppCompatActivity {
    private static final String TAG = ResumeProcessActivity.class.getSimpleName();
    TextInputLayout userNameLayout, phoneLayout, otpLayout;
    TextInputEditText userNameET, phoneET, otpET;
    TextView phoneHint;
    String userName, phone, otp, password;
    ProgressDialog dialog;
    Button next;

    boolean validUser, validPhone, otpVerified = false;

    List<String> userNameFS;
    ArrayList<UserNameItem> nameItems;
    boolean own;
    OnBoardSharedPreference preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resume_process);
        preference = new OnBoardSharedPreference(this);
        getFrestoreCollection();

        userNameLayout = findViewById(R.id.resume_user_name_layout);
        phoneLayout = findViewById(R.id.resume_phone_layout);
        otpLayout = findViewById(R.id.resume_otp_layout);
        userNameET = findViewById(R.id.resume_user_name);
        phoneET = findViewById(R.id.resume_phone);
        otpET = findViewById(R.id.resume_otp);
        next = findViewById(R.id.resume_submit);
        phoneHint = findViewById(R.id.resume_phone_hint);
        validUser = false;
        validPhone = false;

        findViewById(R.id.resume_new_user).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ResumeProcessActivity.this, NewUserActivity.class));
                finish();
            }
        });

        userNameET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validPhone = false;
                userName = userNameET.getText().toString();
                if (userName.length() >= 4) {
                    if (userNameFS.contains(userName)) {
                        int index = userNameFS.indexOf(userName);
                        UserNameItem current = nameItems.get(index);
                        if (String.valueOf(current.getAdminName()) != null || current.getAdminName().equals(OnboardConfig.userAdmin)) {
//                        if (current.getAdminName().equals("demoisu") || current.getAdminName().equals("techadmin")) {
                            userNameLayout.setError(null);
                            validUser = true;
                            phone = current.getPhone();
                            password = current.getPassword();

                            preference.setStringValue(OnboardConfig.USER_NAME_KEY, userName);
                            preference.setStringValue(OnboardConfig.USER_PASSWORD_KEY, password);

                            Log.e(TAG, "onTextChanged: phone " + phone);
                            Log.e(TAG, "onTextChanged: last 4 " + phone.substring(6));
                            phoneHint.setText("Enter your registered phone number : XXXXXX" + phone.substring(6));
                            phoneHint.setVisibility(View.VISIBLE);
                            String p = phoneET.getText().toString();
                            if (!p.equals("")){
                                if (p.equals(phone)){
                                    validPhone = true;
                                    phoneLayout.setError(null);
                                } else {
                                    validPhone = false;
                                    phoneLayout.setError("Enter the registered phone number");
                                }
                            }
                        } else {
                            userNameLayout.setError("User name does not belong to your admin name");
                            validUser = false;
                            phoneHint.setVisibility(View.GONE);
                        }
                    } else {
                        userNameLayout.setError("User name does not exist. Create a new one");
                        validUser = false;
                        phoneHint.setVisibility(View.GONE);
                    }
                } else {
                    userNameLayout.setError("User name must be at least 4 characters");
                    validUser = false;
                    phoneHint.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        phoneET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String p = phoneET.getText().toString();
                if (!p.equals(phone)) {
                    phoneLayout.setError("Enter the registered phone number");
                    validPhone = false;
                } else {
                    validPhone = true;
                    phoneLayout.setError(null);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (otpVerified) {
                    otp = otpET.getText().toString();
                    if (!otp.equals("")) {
                        verifyOtp(phone, otp);
                    } else {
                        hideKeyboard();
                        Validate.showAlert(ResumeProcessActivity.this, "Enter the OTP");
                    }
                } else {
                    if (validPhone && validUser) {
                        sendOTP(phone);
                    } else if (!validUser) {
                        Validate.showAlert(ResumeProcessActivity.this, "Enter the valid user name");
                    } else if (!validPhone) {
                        Validate.showAlert(ResumeProcessActivity.this, "Enter the registered phone number");
                    }
                }

            }
        });

    }

    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


    private void getFrestoreCollection() {
        own = false;
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        CollectionReference uNames = db.collection("SelfUserNames");
        uNames.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot snapshots, @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.e(TAG, "onEvent: error occured");
                }

                if (snapshots.isEmpty()) {
                    Log.e(TAG, "onEvent: empty snap");
                } else {
                    List<DocumentSnapshot> doc = snapshots.getDocuments();
                    userNameFS = new ArrayList<>();
                    nameItems = new ArrayList<>();
                    for (int i = 0; i < doc.size(); i++) {
                        Map<String, Object> userDetails = doc.get(i).getData();
                        Log.e(TAG, doc.get(i).getId() + " => " + doc.get(i).getData());
                        String password = (String) userDetails.get("password");
                        String phone = (String) userDetails.get("phone");
                        String admin = (String) userDetails.get("admin");
                        String st = (String) userDetails.get("status");
                        if (st.equals("0")) {
                            userNameFS.add(doc.get(i).getId());
                            nameItems.add(new UserNameItem(doc.get(i).getId(), password, admin, phone));
                        }
                    }
                }
            }
        });
    }

    private void verifyOtp(final String m, String o) {
        dialog = new ProgressDialog(ResumeProcessActivity.this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();
        JSONObject obj = new JSONObject();

        try {
            obj.put("phoneNumber", m);
            obj.put("otp", o);
            obj.put("type", "phone");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(OnboardConfig.getOtpURL())
                .setPriority(Priority.HIGH)
                .addJSONObjectBody(obj)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e(TAG, "onResponse: response " + response);
                        try {
                            int statusCode = response.getInt("statusCode");
                            if (statusCode == 0) {
                                JSONObject data = response.getJSONObject("data");
                                String status = data.getString("status");
                                if (status.equalsIgnoreCase("success")) {
                                    String statusDesc = data.getString("statusDesc");
                                    preference.setStringValue(OnboardConfig.PHONE_KEY, m);
                                    preference.setStringValue(OnboardConfig.OTP_KEY, o);
//                                    Toast.makeText(ResumeProcessActivity.this, statusDesc, Toast.LENGTH_LONG).show();
//                                    if (OnboardConfig.isIsNewUser()){
//                                        Intent intent = new Intent(ResumeProcessActivity.this, VerifyEmailActivity.class);
//                                        intent.putExtra("USER_TYPE", "new");
//                                        startActivity(intent);
//                                        finish();
//                                    } else {
//
//                                    }

                                    Intent intent = new Intent(ResumeProcessActivity.this, WelcomeActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    finish();

                                } else {
                                    Toast.makeText(ResumeProcessActivity.this, "Try Again", Toast.LENGTH_LONG).show();
                                }
                            }

                            dialog.dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        try {

                            // 0 mismatch
                            // -1 expired
                            // 1 success
                            JSONObject errorObject = new JSONObject(anError.getErrorBody());
                            Log.e(TAG, "onError: response " + errorObject);
                            JSONObject data = errorObject.getJSONObject("data");
                            String statusDesc = data.getString("statusDesc");
//                            Toast.makeText(ResumeProcessActivity.this, statusDesc, Toast.LENGTH_LONG).show();
                            Validate.showAlert(ResumeProcessActivity.this, statusDesc);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        dialog.dismiss();
                    }
                });
    }

    private void sendOTP(String p) {
        dialog = new ProgressDialog(ResumeProcessActivity.this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();

        JSONObject obj = new JSONObject();

        try {
            obj.put("phoneNumber", p);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(OnboardConfig.getOtpURL())
                .setPriority(Priority.HIGH)
                .addJSONObjectBody(obj)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            int statusCode = response.getInt("statusCode");
                            if (statusCode == 0) {
                                JSONObject data = response.getJSONObject("data");
                                String status = data.getString("status");
                                if (status.equalsIgnoreCase("success")) {
                                    String statusDesc = data.getString("statusDesc");
                                    Toast.makeText(ResumeProcessActivity.this, statusDesc, Toast.LENGTH_LONG).show();
                                    otpVerified = true;
                                    next.setText("PROCEED");
                                    otpLayout.setVisibility(View.VISIBLE);
                                } else {
                                    Toast.makeText(ResumeProcessActivity.this, "Try Again", Toast.LENGTH_LONG).show();
                                }
                            }

                            dialog.dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        try {

                            // 0 mismatch
                            // -1 expired
                            // 1 success
                            JSONObject errorObject = new JSONObject(anError.getErrorBody());
                            JSONObject data = errorObject.getJSONObject("data");
                            String statusDesc = data.getString("statusDesc");
//                            Toast.makeText(ResumeProcessActivity.this, statusDesc, Toast.LENGTH_LONG).show();
                            Validate.showAlert(ResumeProcessActivity.this, statusDesc);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        dialog.dismiss();
                    }
                });
    }


}