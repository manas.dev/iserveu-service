package com.isu.service.onboarding.ekyc;

import android.content.Intent;
import android.os.Bundle;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.isu.service.MainActivity;
import com.isu.service.R;
import com.isu.service.onboarding.configuration.OnBoardSharedPreference;
import com.isu.service.onboarding.configuration.OnboardConfig;
import com.isu.service.onboarding.configuration.Validate;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class EkycRequestActivity extends AppCompatActivity {
    private static final String TAG = EkycRequestActivity.class.getSimpleName();
    OnBoardSharedPreference preference;

    ProgressBar progressBar;
    Button proceed;
    TextView messageText;
    LinearLayout parentLayout;

    boolean isSuccess, newCreated;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ekyc_request);
        OnboardConfig.whiteStatusNav(this);
        preference = new OnBoardSharedPreference(this);

        progressBar = findViewById(R.id.kyc_request_progress);
        proceed = findViewById(R.id.ekyc_form_next);
        messageText = findViewById(R.id.kyc_request_message);
        parentLayout = findViewById(R.id.kyc_request_parent);

        if (OnboardConfig.isIsNewUser()){
            newCreated = false;
            requestToAdd();
        } else {
            newCreated = true;
            verifyKYC();
        }

        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (newCreated) {
                    if (isSuccess) {
                        Log.e(TAG, "onClick: is success" );
                        //make login
                        Intent intent = new Intent(EkycRequestActivity.this, MainActivity.class);
                        intent.putExtra("ActivityName", "CompleteOB");
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    } else {
                        Log.e(TAG, "onClick: is success false" );
                        TransitionManager.beginDelayedTransition(parentLayout);
                        progressBar.setVisibility(View.VISIBLE);
                        proceed.setVisibility(View.GONE);
                        messageText.setVisibility(View.GONE);
                        verifyKYC();
                    }
                } else {
                    Log.e(TAG, "onClick: is new created false" );
                    Intent intent = new Intent(EkycRequestActivity.this, MainActivity.class);
                    intent.putExtra("ActivityName", "CompleteOB");
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }
            }
        });

    }

    private void requestToAdd() {
        progressBar.setVisibility(View.VISIBLE);

        JSONObject obj = new JSONObject();
        try {
//            obj.put("user_name", "aepsTestR");
            obj.put("userName", preference.getStringValue(OnboardConfig.USER_NAME_KEY));
            obj.put("password", preference.getStringValue(OnboardConfig.USER_PASSWORD_KEY));
            obj.put("userType", "RETAILER");
            obj.put("firstName", preference.getStringValue(OnboardConfig.FIRST_NAME_KEY));
            obj.put("lastName", preference.getStringValue(OnboardConfig.LAST_NAME_KEY));
            obj.put("mobileNumber", preference.getStringValue(OnboardConfig.PHONE_KEY));
            obj.put("email", preference.getStringValue(OnboardConfig.MAIL_KEY));
            obj.put("city", preference.getStringValue(OnboardConfig.CITY_KEY));
            obj.put("state", preference.getStringValue(OnboardConfig.STATE_KEY));
            obj.put("address", preference.getStringValue(OnboardConfig.ADDRESS_KEY));
            obj.put("panCard", preference.getStringValue(OnboardConfig.PAN_CARD_KEY));
            obj.put("adharCard", preference.getStringValue(OnboardConfig.AADHAAR_KEY));
            obj.put("shopName", preference.getStringValue(OnboardConfig.SHOP_KEY));
            obj.put("otp", preference.getStringValue(OnboardConfig.OTP_KEY));
            obj.put("pincode", preference.getStringValue(OnboardConfig.PIN_KEY));
            obj.put("createdBy", OnboardConfig.createBy);
//            obj.put("createdBy", "demoisu");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e(TAG, "requestToAdd: obj "+obj );

        AndroidNetworking.post(OnboardConfig.getNewUser())
                .setPriority(Priority.HIGH)
                .addJSONObjectBody(obj)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e(TAG, "onResponse: "+response );
                        try {
                            String status = response.getString("status");
                            if (status.equals("0")){
                                newCreated = true;
                                storeSubStatus();
                            } else {
                                newCreated = false;
                                String desc = response.getString("statusDesc");
                                TransitionManager.beginDelayedTransition(parentLayout);
                                progressBar.setVisibility(View.GONE);
                                messageText.setVisibility(View.VISIBLE);
                                proceed.setVisibility(View.VISIBLE);
                                proceed.setText("RETRY");
                                isSuccess = false;
                                messageText.setText(desc);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e(TAG, "onError: error "+anError.getErrorBody() );
                        newCreated = false;
                        try {
                            JSONObject errorObject = new JSONObject(anError.getErrorBody());
                            String desc = errorObject.getString("statusDesc");
                            TransitionManager.beginDelayedTransition(parentLayout);
                            progressBar.setVisibility(View.GONE);
                            messageText.setVisibility(View.VISIBLE);
                            proceed.setVisibility(View.VISIBLE);
                            proceed.setText("RETRY");
                            isSuccess = false;
                            messageText.setText(desc);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
    }

    private void verifyKYC() {
        progressBar.setVisibility(View.VISIBLE);

        JSONObject obj = new JSONObject();
        try {
//            obj.put("user_name", "aepsTestR");
            obj.put("user_name", preference.getStringValue(OnboardConfig.USER_NAME_KEY));
            obj.put("kyc_status", "1");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(OnboardConfig.getActivateUserURL())
                .setPriority(Priority.HIGH)
                .addJSONObjectBody(obj)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String status = response.getString("statusCode");
                            if (status.equals("0")) {
                                storeSubStatus();
//                                finish();
                            } else {
                                TransitionManager.beginDelayedTransition(parentLayout);
                                progressBar.setVisibility(View.GONE);
                                messageText.setVisibility(View.VISIBLE);
                                proceed.setVisibility(View.VISIBLE);
                                proceed.setText("RETRY");
                                isSuccess = false;
                                messageText.setText(response.toString());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            TransitionManager.beginDelayedTransition(parentLayout);
                            progressBar.setVisibility(View.GONE);
                            messageText.setVisibility(View.VISIBLE);
                            proceed.setVisibility(View.VISIBLE);
                            proceed.setText("RETRY");
                            isSuccess = false;
                            messageText.setText(e.getMessage());
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        try {
                            JSONObject errorObject = new JSONObject(anError.getErrorBody());
                            Toast.makeText(EkycRequestActivity.this, errorObject.getJSONObject("data").getString("statusDesc"), Toast.LENGTH_LONG).show();
                            messageText.setText(errorObject.getJSONObject("data").getString("statusDesc"));
                            TransitionManager.beginDelayedTransition(parentLayout);
                            progressBar.setVisibility(View.GONE);
                            messageText.setVisibility(View.VISIBLE);
                            proceed.setVisibility(View.VISIBLE);
                            proceed.setText("RETRY");
                            isSuccess = false;
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(EkycRequestActivity.this, "Please try later", Toast.LENGTH_LONG).show();
                            TransitionManager.beginDelayedTransition(parentLayout);
                            progressBar.setVisibility(View.GONE);
                            messageText.setVisibility(View.VISIBLE);
                            proceed.setVisibility(View.VISIBLE);
                            proceed.setText("RETRY");
                            isSuccess = false;
                            messageText.setText(e.getMessage());
                        }
                    }
                });
    }

    private void storeSubStatus() {


        if (OnboardConfig.isIsNewUser()) {
            updateStatus(preference.getStringValue(OnboardConfig.USER_NAME_KEY));
        }

        JSONObject obj = new JSONObject();

        try {
            obj.put("type", "STATUS_UPDATE");
            obj.put("username", preference.getStringValue(OnboardConfig.USER_NAME_KEY));
            obj.put("subStatus", "6");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(OnboardConfig.getSaveUrl())
                .setPriority(Priority.HIGH)
                .addJSONObjectBody(obj)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e(TAG, "onResponse: " + response);
                        TransitionManager.beginDelayedTransition(parentLayout);
                        progressBar.setVisibility(View.GONE);
                        messageText.setVisibility(View.VISIBLE);
                        proceed.setVisibility(View.VISIBLE);
                        isSuccess = true;
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e(TAG, "onError: body " + anError.getErrorBody());
                        Log.e(TAG, "onError: details " + anError.getErrorDetail());
                        TransitionManager.beginDelayedTransition(parentLayout);
                        progressBar.setVisibility(View.GONE);
                        messageText.setVisibility(View.VISIBLE);
                        proceed.setVisibility(View.VISIBLE);
                        isSuccess = true;
                    }
                });
    }

    public void updateStatus(String name){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        CollectionReference uNames = db.collection("SelfUserNames");
        Map<String, Object> userDetails = new HashMap<>();
        userDetails.put("password", preference.getStringValue(OnboardConfig.USER_PASSWORD_KEY));
        userDetails.put("phone", preference.getStringValue(OnboardConfig.PHONE_KEY));
        userDetails.put("admin", OnboardConfig.userAdmin);
        userDetails.put("status", "1");

        uNames.document(name).set(userDetails)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.e(TAG, "DocumentSnapshot successfully written!" + userDetails);
//                        Intent intent = new Intent(EkycRequestActivity.this, WelcomeActivity.class);
//                        startActivity(intent);
//                        finish();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e(TAG, "Error writing document", e);
//                Toast.makeText(EkycRequestActivity.this, "Try Again Later!", Toast.LENGTH_LONG).show();
            }
        });

    }

    @Override
    public void onBackPressed() {
        Validate.showExitAlert(this);
    }


}