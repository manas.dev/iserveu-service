package com.isu.service.onboarding;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.isu.service.R;
import com.isu.service.onboarding.configuration.OnboardConfig;
import com.isu.service.onboarding.userType.NewUserActivity;
import com.isu.service.onboarding.userType.ResumeProcessActivity;

import static android.content.ContentValues.TAG;

public class UserTypeActivity extends AppCompatActivity {
    int selected = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_type);
        selected = 0;
        AlertDialog.Builder builder = new AlertDialog.Builder(UserTypeActivity.this);
        builder.setTitle("Select a process");
        String[] options = {"Resume Process", "Create New User"};
        builder.setSingleChoiceItems(options, selected, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                selected = which;
            }
        });
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int pos) {
                OnboardConfig.setIsNewUser(true);
                Log.e(TAG, "onClick: " + selected);
                if (selected == 0) { // Resume Process
                    startActivity(new Intent(UserTypeActivity.this, ResumeProcessActivity.class));
                    finish();
                } else if (selected == 1) { //Create New User
                    startActivity(new Intent(UserTypeActivity.this, NewUserActivity.class));
                    finish();
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        builder.setCancelable(false);
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}