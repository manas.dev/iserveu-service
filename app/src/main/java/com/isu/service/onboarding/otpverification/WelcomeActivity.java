package com.isu.service.onboarding.otpverification;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.isu.service.MainActivity;
import com.isu.service.R;
import com.isu.service.onboarding.configuration.OnBoardSharedPreference;
import com.isu.service.onboarding.configuration.OnboardConfig;
import com.isu.service.onboarding.configuration.Validate;
import com.isu.service.onboarding.digilocker.DigilockerActivity;
import com.isu.service.onboarding.ekyc.EkycAgreementActivity;
import com.isu.service.onboarding.ekyc.EkycFormActivity;
import com.isu.service.onboarding.ekyc.EkycRequestActivity;
import com.isu.service.onboarding.ekyc.PanVerificationActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class WelcomeActivity extends AppCompatActivity {
    private static final String TAG = WelcomeActivity.class.getSimpleName();
    ProgressDialog dialog;
    OnBoardSharedPreference preference;
    String phone;
    Map<String, Object> mapObject = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        OnboardConfig.whiteStatusNav(this);

        preference = new OnBoardSharedPreference(this);
        phone = preference.getStringValue(OnboardConfig.PHONE_KEY);

        String u = preference.getStringValue(OnboardConfig.USER_NAME_KEY);
        Log.e(TAG, "onCreate: user name key " + u);
//        u = "itpl";

        FirebaseFirestore db = FirebaseFirestore.getInstance();
//        Map<String, Object> mapObject = OnboardConfig.getFirestoreData(this, db, u);

        /*
        DocumentReference docRef = db.collection("NewUserInfoCollection").document(u);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    if (task.getResult().getData() != null) {
                        Log.e(TAG, "onComplete: has data status " + task.getResult().getData());
                        mapObject = task.getResult().getData();
                        Log.e(TAG, "onComplete: map " + mapObject);
                        OnboardConfig.setDocument(mapObject);
                    } else {
                        mapObject = null;
                    }
                } else {
                    Log.e(TAG, "onComplete: error fetching data");
                }
            }

        });

        docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                Log.e(TAG, "onSuccess: " + documentSnapshot.getData());
            }
        });
        */

        findViewById(R.id.welcome_proceed).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Log.e(TAG, "onClick: empty map "+mapObject.isEmpty() );
//                generateUserID();
                kycFetch(preference.getStringValue(OnboardConfig.USER_NAME_KEY));
            }
        });
    }

    private void kycFetch(String name) {
        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();

        JSONObject obj = new JSONObject();
        try {
            obj.put("username", name);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(OnboardConfig.getFetchUrl())
                .setPriority(Priority.HIGH)
                .addJSONObjectBody(obj)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e(TAG, "onResponse: " + response);
                        try {
                            String status = response.getString("status");
                            if (status.equals("0")) {
                                JSONObject data = response.getJSONObject("data");
                                String subStatus = data.getString("SUB_STATUS");
                                OnboardConfig.setDocObject(data);
                                if (data.has("basicInfo")) {
                                    Log.e(TAG, "onResponse: data has info " + data.getJSONObject("basicInfo"));
                                    JSONObject basicInfo = data.getJSONObject("basicInfo");
                                    String name = basicInfo.getString("NAME");
                                    String phone = basicInfo.getString("PHONE");
                                    String email = basicInfo.getString("EMAIL");
                                    String city = basicInfo.getString("CITY");
                                    String state = basicInfo.getString("STATE");
                                    String address = basicInfo.getString("ADDRESS");
                                    String adharCard = basicInfo.getString("AADHAAR");
                                    String shopName = basicInfo.getString("SHOP");
                                    String pincode = basicInfo.getString("PINCODE");

                                    String surName = name.split(" ")[name.split(" ").length - 1];
                                    String firstName = name.substring(0, name.length() - surName.length()).trim();
                                    if (surName.equals("")){
                                        surName = "N/A";
                                    }
                                    preference.setStringValue(OnboardConfig.FIRST_NAME_KEY, firstName);
                                    preference.setStringValue(OnboardConfig.LAST_NAME_KEY, surName);
                                    preference.setStringValue(OnboardConfig.SHOP_KEY, shopName);
                                    preference.setStringValue(OnboardConfig.AADHAAR_KEY, adharCard);
                                    preference.setStringValue(OnboardConfig.MAIL_KEY, email);
                                    preference.setStringValue(OnboardConfig.CITY_KEY, city);
                                    preference.setStringValue(OnboardConfig.STATE_KEY, state);
                                    preference.setStringValue(OnboardConfig.ADDRESS_KEY, address);
                                    preference.setStringValue(OnboardConfig.PIN_KEY, pincode);

                                }
                                if (subStatus.equals("1")) {
                                    // status == 1, skip basic details and load rbl link
                                    Intent i = new Intent(WelcomeActivity.this, EkycFormActivity.class);
                                    i.putExtra("data", "1");


//                                    Intent i = new Intent(WelcomeActivity.this, RblLinkActivity.class);
//                                    String cpuniquerefno = data.getString("cpuniquerefno");
//                                    String url = data.getString("url");
//                                    i.putExtra("url", url + "&cpuniquerefno=" + cpuniquerefno);

//                                    i.putExtra("data", "1");

                                    Log.e(TAG, "onResponse: data " + data);
                                    dialog.dismiss();
                                    startActivity(i);
                                    finish();
                                } else if (subStatus.equals("2")) {
                                    // status == 2, skip rbl url and get requery and set

                                    String clientRefNo = data.getString("clientRefNo");
                                    preference.setStringValue(OnboardConfig.CLIENT_REF_KEY, clientRefNo);

                                    JSONObject basicInfo = data.getJSONObject("basicInfo");
                                    String mapName = basicInfo.getString("NAME");
                                    preference.setStringValue(OnboardConfig.NAME_KEY, mapName);

                                    Intent i = new Intent(WelcomeActivity.this, PanVerificationActivity.class);
                                    dialog.dismiss();
                                    startActivity(i);
                                    finish();
                                } else if (subStatus.equals("3")) {
                                    // status == 3, load pan info activity,
                                    JSONObject basicInfo = data.getJSONObject("basicInfo");
                                    String mapName = basicInfo.getString("NAME");
                                    preference.setStringValue(OnboardConfig.NAME_KEY, mapName);

                                    Intent intent = new Intent(WelcomeActivity.this, PanVerificationActivity.class);
                                    dialog.dismiss();
                                    startActivity(intent);
                                    finish();
                                } else if (subStatus.equals("4")) {
                                    // status == 4, get pan info from firestore, try matching
                                    JSONObject basicInfo = data.getJSONObject("basicInfo");
                                    String mapName = basicInfo.getString("NAME");
                                    preference.setStringValue(OnboardConfig.NAME_KEY, mapName);

                                    Intent intent = new Intent(WelcomeActivity.this, PanVerificationActivity.class);
                                    dialog.dismiss();
                                    startActivity(intent);
                                    finish();
                                } else if (subStatus.equals("5")) {
                                    // status == 5, load kyc request and complete kyc
                                    JSONObject panObject = data.getJSONObject("PanInfo");
                                    String pan = panObject.getString("PAN_NUMBER");
                                    preference.setStringValue(OnboardConfig.PAN_CARD_KEY, pan);
                                    Intent intent = new Intent(WelcomeActivity.this, EkycRequestActivity.class);
                                    dialog.dismiss();
                                    startActivity(intent);
                                    finish();
                                } else if (subStatus.equals("6")){

                                    dialog.dismiss();
                                    String msg = "EKYC process has been completed for the user " + preference.getStringValue(OnboardConfig.USER_NAME_KEY) +
                                            "\nPlease make the login";
                                    Toast.makeText(WelcomeActivity.this, msg, Toast.LENGTH_LONG).show();

                                    Intent intent = new Intent(WelcomeActivity.this, MainActivity.class);
                                    intent.putExtra("ActivityName", "CompleteOB");
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    finish();

                                } else if (subStatus.equals("7")){
                                    Intent intent = new Intent(WelcomeActivity.this, DigilockerActivity.class);
                                    intent.putExtra("digiUrl", "refetchDigiLocker");
                                    intent.putExtra("digiUser", "n");
                                    dialog.dismiss();
                                    startActivity(intent);
                                    finish();
                                }
                            }
                            dialog.dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        startActivity(new Intent(WelcomeActivity.this, EkycAgreementActivity.class));
                        finish();
                        dialog.dismiss();
                    }
                });


    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Validate.showExitAlert(this);
    }
}
