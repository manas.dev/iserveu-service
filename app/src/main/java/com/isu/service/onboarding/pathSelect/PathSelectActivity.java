package com.isu.service.onboarding.pathSelect;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.isu.service.R;
import com.isu.service.onboarding.configuration.OnBoardSharedPreference;
import com.isu.service.onboarding.configuration.OnboardConfig;
import com.isu.service.onboarding.configuration.Validate;
import com.isu.service.onboarding.digilocker.DigiPanActivity;
import com.isu.service.onboarding.digilocker.DigilockerActivity;
import com.isu.service.onboarding.ekyc.EkycRequestActivity;
import com.isu.service.onboarding.ekyc.RblLinkActivity;

import org.json.JSONException;
import org.json.JSONObject;

public class PathSelectActivity extends AppCompatActivity {
    private static final String TAG = PathSelectActivity.class.getSimpleName();

    OnBoardSharedPreference preference;
    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_path_select);
        OnboardConfig.whiteStatusNav(this);
        preference = new OnBoardSharedPreference(this);

//        preference.setStringValue(OnboardConfig.USER_NAME_KEY, "FJIYO");

        findViewById(R.id.path_ekyc).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startRBL(preference.getStringValue(OnboardConfig.FULL_NAME_KEY), preference.getStringValue(OnboardConfig.DATE_KEY), preference.getStringValue(OnboardConfig.ADDRESS_KEY), preference.getStringValue(OnboardConfig.PIN_KEY), preference.getStringValue(OnboardConfig.STATE_KEY), preference.getStringValue(OnboardConfig.CITY_KEY), preference.getStringValue(OnboardConfig.PHONE_KEY), preference.getStringValue(OnboardConfig.MAIL_KEY), preference.getStringValue(OnboardConfig.AADHAAR_KEY), preference.getStringValue(OnboardConfig.SHOP_KEY));
//                Intent i = new Intent(PathSelectActivity.this, PanVerificationActivity.class);
//                startActivity(i);
            }
        });

        findViewById(R.id.path_digilocker).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                String name = generateName(5);
//                Log.e(TAG, "onClick: generated name " + name);
                fetchDigiLocker();
            }
        });
    }

    private void startRBL(String genName, String gendob, String genAddress, String genPin, String genState, String genCity, String genPhone, String genMail, String genAadhaar, String genShop) {
        dialog = new ProgressDialog(PathSelectActivity.this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();

        JSONObject obj = new JSONObject();

        try {
            JSONObject dataObject = new JSONObject();
            dataObject.put("redirUrl", "android");
            dataObject.put("username", preference.getStringValue(OnboardConfig.USER_NAME_KEY));
            dataObject.put("name", genName);
            dataObject.put("dob", gendob);
            dataObject.put("address", genAddress);
            dataObject.put("city", genCity);
            dataObject.put("state", genState);
            dataObject.put("pin", genPin);
            dataObject.put("email", genMail);
            dataObject.put("phone", genPhone);
            dataObject.put("shop", genShop);
            dataObject.put("aadhaar", genAadhaar);

            obj.put("subStatus", "1");
            obj.put("type", "BASIC_INFO");
//            obj.put("type", "INFO");
            obj.put("username", preference.getStringValue(OnboardConfig.USER_NAME_KEY));
            obj.put("data", dataObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e(TAG, "storeBasicInfo: obj " + obj);

        AndroidNetworking.post(OnboardConfig.getSaveUrl())
                .setPriority(Priority.HIGH)
                .addJSONObjectBody(obj)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e(TAG, "onResponse: " + response);
                        try {
                            String status = response.getString("status");
                            if (status.equals("0")) {
//                                Intent intent = new Intent(PathSelectActivity.this, PathSelectActivity.class);
                                JSONObject data = response.getJSONObject("data");
                                String clientRefNo = response.getString("clientRefNo");
                                preference.setStringValue(OnboardConfig.CLIENT_REF_KEY, clientRefNo);
                                String cpuniquerefno = data.getString("cpuniquerefno");
                                String url = data.getString("url");
                                Intent intent = new Intent(PathSelectActivity.this, RblLinkActivity.class);
                                intent.putExtra("url", url + "&cpuniquerefno=" + cpuniquerefno);
                                dialog.dismiss();
                                startActivity(intent);
                                finish();
                            } else {
                                Toast.makeText(PathSelectActivity.this, "Please try again", Toast.LENGTH_LONG).show();
                                dialog.dismiss();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(PathSelectActivity.this, "Please try again", Toast.LENGTH_LONG).show();
                            dialog.dismiss();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e(TAG, "onError: body " + anError.getErrorBody());
                        Log.e(TAG, "onError: details " + anError.getErrorDetail());
                        try {
                            JSONObject errorObject = new JSONObject(anError.getErrorBody());
                            Toast.makeText(PathSelectActivity.this, errorObject.getString("statusDesc"), Toast.LENGTH_LONG).show();
                            dialog.dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(PathSelectActivity.this, "Please try again later!", Toast.LENGTH_LONG).show();
                            dialog.dismiss();
                        }
                    }
                });

    }

    private void getDigilocker(String n) {

        String url = OnboardConfig.getDigiLockerUrl() + n;
//        String url = OnboardConfig.getDigiLockerUrl()+preference.getStringValue(OnboardConfig.USER_NAME_KEY);

        AndroidNetworking.get(url)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e(TAG, "onResponse: " + response);
                        try {
                            String digiUrl = response.getString("digiUrl");
                            Intent intent = new Intent(PathSelectActivity.this, DigilockerActivity.class);
                            intent.putExtra("digiUrl", digiUrl);
                            intent.putExtra("digiUser", n);
                            startActivity(intent);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        dialog.dismiss();
                    }

                    @Override
                    public void onError(ANError anError) {
                        try {
                            JSONObject errorObject = new JSONObject(anError.getErrorBody());
                            Log.e(TAG, "onError: " + errorObject);
                            dialog.dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
    }

    void fetchDigiLocker() {
        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();

        JSONObject obj = new JSONObject();

        try {
            obj.put("username", preference.getStringValue(OnboardConfig.USER_NAME_KEY));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e(TAG, "fetchDigiLocker: obj " + obj);
        AndroidNetworking.post(OnboardConfig.getDigiFetchUrl())
                .setPriority(Priority.HIGH)
                .addJSONObjectBody(obj)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e(TAG, "onResponse: " + response);
                        try {
                            String status = response.getString("status");
                            if (status.equals("0")) {
                                JSONObject data = response.getJSONObject("data");
                                String name = data.getString("name");
//                                digiText = digiText+"\nName \t\t: "+name;
//                                if (data.has("ADHAR")){
//                                    digiText = digiText+"\nAadhaar \t\t: "+data.getString("ADHAR");
//                                }
//                                if (data.has("PANCR")){
//                                    digiText = digiText+"\nPancard \t\t: "+data.getString("PANCR");
//                                }

                                if (!data.has("ADHAR")) {
//                                    Validate.showAlert(PathSelectActivity.this, "Please add your Aadhaar Card to Digi-Locker.");
                                    getDigilocker(preference.getStringValue(OnboardConfig.USER_NAME_KEY));
                                }
                                if (!data.has("PANCR")) {
                                    String p = data.getString("pancard");
                                    if (p.equals("")) {

                                        getDigilocker(preference.getStringValue(OnboardConfig.USER_NAME_KEY));
/*

                                        showAlertDialog("PAN card not found");
                                        String surName = name.split(" ")[name.split(" ").length - 1];
                                        String firstName = name.substring(0, name.length() - surName.length());

                                        if (surName.equals("")) {
                                            surName = "N/A";
                                        }

                                        preference.setStringValue(OnboardConfig.PAN_CARD_KEY, p);
                                        preference.setStringValue(OnboardConfig.AADHAAR_KEY, data.getString("ADHAR"));
                                        preference.setStringValue(OnboardConfig.FULL_NAME_KEY, name);
                                        preference.setStringValue(OnboardConfig.FIRST_NAME_KEY, firstName);
                                        preference.setStringValue(OnboardConfig.LAST_NAME_KEY, surName);

 */


                                    } else {
                                        String surName = name.split(" ")[name.split(" ").length - 1];
                                        String firstName = name.substring(0, name.length() - surName.length());

                                        if (surName.equals("")) {
                                            surName = "N/A";
                                        }

//                                        preference.setStringValue(OnboardConfig.PAN_CARD_KEY, p);
//                                        preference.setStringValue(OnboardConfig.AADHAAR_KEY, data.getString("ADHAR"));
//                                        preference.setStringValue(OnboardConfig.FULL_NAME_KEY, name);
//                                        preference.setStringValue(OnboardConfig.FIRST_NAME_KEY, firstName);
//                                        preference.setStringValue(OnboardConfig.LAST_NAME_KEY, surName);

                                        Intent i = new Intent(PathSelectActivity.this, EkycRequestActivity.class);
                                        startActivity(i);
                                        finish();
                                    }
                                } else {

                                    String surName = name.split(" ")[name.split(" ").length - 1];
                                    String firstName = name.substring(0, name.length() - surName.length());
                                    if (surName.equals("")) {
                                        surName = "N/A";
                                    }

//                                    preference.setStringValue(OnboardConfig.PAN_CARD_KEY, data.getString("PANCR"));
//                                    preference.setStringValue(OnboardConfig.AADHAAR_KEY, data.getString("ADHAR"));
//                                    preference.setStringValue(OnboardConfig.FULL_NAME_KEY, name);
//                                    preference.setStringValue(OnboardConfig.FIRST_NAME_KEY, firstName);
//                                    preference.setStringValue(OnboardConfig.LAST_NAME_KEY, surName);

                                    Intent i = new Intent(PathSelectActivity.this, EkycRequestActivity.class);
                                    startActivity(i);
                                    finish();
                                }

                                dialog.dismiss();
                            } else {
                                getDigilocker(preference.getStringValue(OnboardConfig.USER_NAME_KEY));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e(TAG, "onResponse: json exp " + e);
                            getDigilocker(preference.getStringValue(OnboardConfig.USER_NAME_KEY));
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e(TAG, "onError: body " + anError.getErrorBody());
                        Log.e(TAG, "onError: details " + anError.getErrorDetail());
                        getDigilocker(preference.getStringValue(OnboardConfig.USER_NAME_KEY));
//                        dialog.dismiss();
                    }
                });

    }

    private void showAlertDialog(String statusDesc) {
        boolean installed = appInstalledOrNot("com.digilocker.android");
        try {
            AlertDialog.Builder alertbuilderupdate;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                alertbuilderupdate = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
            } else {
                alertbuilderupdate = new AlertDialog.Builder(this);
            }
            alertbuilderupdate.setCancelable(false);
            // String message = "Session is already running !!! Please login after sometimes.";
            alertbuilderupdate.setTitle("Alert")
                    .setMessage(statusDesc)
                    .setPositiveButton("Upload PAN Card", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Intent intent = new Intent(PathSelectActivity.this, DigiPanActivity.class);
                            startActivity(intent);
                            dialog.cancel();
                        }
                    });
            alertbuilderupdate.setNegativeButton(
                    "Add PAN Card via Digi-Locker",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            try {
                                if (installed) {
                                    PackageManager manager = getPackageManager();
                                    Intent sIntent = manager.getLaunchIntentForPackage("com.digilocker.android");

                                    sIntent.addCategory(Intent.CATEGORY_LAUNCHER);
                                    startActivity(sIntent);
                                    finish();

                                } else {
                                    showInstallAlert(PathSelectActivity.this);
                                }
                            } catch (Exception e) {

                            }
                            dialog.cancel();
                        }
                    });
            alertbuilderupdate.setNeutralButton(
                    "Cancel",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert11 = alertbuilderupdate.create();
            alert11.show();
        } catch (Exception e) {

        }

    }

    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }
        return false;
    }

    public void showInstallAlert(Context context) {
        try {

            androidx.appcompat.app.AlertDialog.Builder alertbuilderupdate;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                alertbuilderupdate = new androidx.appcompat.app.AlertDialog.Builder(context, android.R.style.Theme_Material_Light_Dialog_Alert);
            } else {
                alertbuilderupdate = new androidx.appcompat.app.AlertDialog.Builder(context);
            }
            alertbuilderupdate.setCancelable(false);
            String message = "Please download the DigiLocker app from the playstore.";
            alertbuilderupdate.setTitle("Alert")
                    .setMessage(message)
                    .setPositiveButton("Download Now", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            redirectToPlayStore();
                        }
                    })
                    .setNegativeButton("Not Now", (dialog, which) -> {
                        dialog.dismiss();
                    });
//                    .show();
            androidx.appcompat.app.AlertDialog alert11 = alertbuilderupdate.create();
            alert11.show();

        } catch (Exception e) {

        }
    }

    public void redirectToPlayStore() {
        Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=com.digilocker.android&hl=en_US");
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=com.digilocker.android&hl=en_US")));
        }
    }

    public String generateName(int n) {
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "0123456789";
        StringBuilder sb = new StringBuilder(n);

        for (int i = 0; i < n; i++) {
            int index = (int) (AlphaNumericString.length() * Math.random());
            sb.append(AlphaNumericString.charAt(index));
        }

        return sb.toString();
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Validate.showExitAlert(this);
    }

}
