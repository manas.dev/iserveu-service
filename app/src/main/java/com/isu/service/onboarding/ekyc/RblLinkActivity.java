package com.isu.service.onboarding.ekyc;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.isu.service.R;
import com.isu.service.onboarding.configuration.OnBoardSharedPreference;
import com.isu.service.onboarding.configuration.OnboardConfig;
import com.isu.service.onboarding.configuration.Validate;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class RblLinkActivity extends AppCompatActivity {
    private static final String TAG = RblLinkActivity.class.getSimpleName();
    WebView webView;
    JSONObject userObject;
    OnBoardSharedPreference preference;

    String clientRefNo;
    ProgressDialog dialog;

    Map<String, Object> document;
    ProgressBar proressV;
    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rbl_link);
        OnboardConfig.whiteStatusNav(this);
        webView = findViewById(R.id.rbl_webview);
        proressV = findViewById(R.id.proressV);
        preference = new OnBoardSharedPreference(this);

//        setFireStoreData();

        String url = getIntent().getStringExtra("url");
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);

                Log.e(TAG, "onPageStarted: loading " + url);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                Log.e(TAG, "onPageFinished: finish " + url);
                super.onPageFinished(view, url);
                proressV.setVisibility(View.GONE);
            }

            @Override
            public void onLoadResource(WebView view, String url) {
                // TODO Auto-generated method stub
                super.onLoadResource(view, url);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.e(TAG, "shouldOverrideUrlLoading: clicked on " + url);
                if (url.contains("android")) {
                    webView.setVisibility(View.INVISIBLE);
                    /*
                    document = OnboardConfig.getDocument();
                    document.put("SUB_STATUS", "2");
                    FirebaseFirestore db = FirebaseFirestore.getInstance();
                    db.collection("NewUserInfoCollection").document(preference.getStringValue(OnboardConfig.USER_NAME_KEY))
                            .set(document)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    OnboardConfig.setDocument(document);
                                    Intent intent = new Intent(RblLinkActivity.this, PanVerificationActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Toast.makeText(RblLinkActivity.this, "Error uploading file\nTry again.", Toast.LENGTH_LONG);
                                    Log.e(TAG, "Error writing document", e);
                                }
                            });
                     */

                    storeSubStatus();
                }
                return super.shouldOverrideUrlLoading(view, url);
            }
        });
        webView.loadUrl(url);
        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        webView.cancelLongPress();

    }

    private void storeSubStatus() {
        dialog = new ProgressDialog(RblLinkActivity.this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();

        JSONObject obj = new JSONObject();

        try {
            obj.put("type", "STATUS_UPDATE");
            obj.put("username", preference.getStringValue(OnboardConfig.USER_NAME_KEY));
            obj.put("subStatus", "2");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(OnboardConfig.getSaveUrl())
                .setPriority(Priority.HIGH)
                .addJSONObjectBody(obj)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e(TAG, "onResponse: " + response);
                        kycFetch(preference.getStringValue(OnboardConfig.USER_NAME_KEY));
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e(TAG, "onError: body " + anError.getErrorBody());
                        Log.e(TAG, "onError: details " + anError.getErrorDetail());
                        Toast.makeText(RblLinkActivity.this, "Please try again.", Toast.LENGTH_LONG);
                    }
                });
    }

    private void kycFetch(String name) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("username", name);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(OnboardConfig.getFetchUrl())
                .setPriority(Priority.HIGH)
                .addJSONObjectBody(obj)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e(TAG, "onResponse: " + response);
                        try {
                            String status = response.getString("status");
                            dialog.dismiss();
                            if (status.equals("0")) {
                                JSONObject data = response.getJSONObject("data");
                                String clientRefNo = data.getString("clientRefNo");
                                preference.setStringValue(OnboardConfig.CLIENT_REF_KEY, clientRefNo);
                                OnboardConfig.setDocObject(data);
                                Intent intent = new Intent(RblLinkActivity.this, PanVerificationActivity.class);
                                startActivity(intent);
                                finish();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        Intent intent = new Intent(RblLinkActivity.this, PanVerificationActivity.class);
                        dialog.dismiss();
                    }
                });


    }

/*    private void setFireStoreData() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        Map<String, Object> document = new HashMap<>();
        Map<String, Object> basicInfo = new HashMap<>();
        try {
            JSONObject object = new JSONObject(getIntent().getStringExtra("userData"));
            basicInfo.put("ADDRESS", object.get("address"));
            basicInfo.put("CITY", object.get("city"));
            basicInfo.put("DOB", object.get("dob"));
            basicInfo.put("EMAIL", object.get("email"));
            basicInfo.put("NAME", object.get("name"));
            basicInfo.put("PAN_NO", getIntent().getStringExtra("pan"));
            basicInfo.put("PHONE", object.get("mobile"));
            basicInfo.put("PINCODE", object.get("pincode"));
            basicInfo.put("STATE", object.get("state"));
            basicInfo.put("USER_ID", getIntent().getStringExtra("userID"));
            basicInfo.put("USER_NAME", preference.getStringValue(OnboardConfig.USER_NAME_KEY));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        document.put("SUB_STATUS", "1");
        document.put("basicInfo", basicInfo);

        db.collection("NewUserInfoCollection").document(preference.getStringValue(OnboardConfig.USER_NAME_KEY))
                .set(document)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.e(TAG, "DocumentSnapshot successfully written!");
                        OnboardConfig.setDocument(document);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e(TAG, "Error writing document", e);
                    }
                });

    }
 */

    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack();
        } else {
//            super.onBackPressed();
            Validate.showExitAlert(this);
        }
    }
}