package com.isu.service.onboarding.digilocker;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.isu.service.R;
import com.isu.service.onboarding.configuration.OnBoardSharedPreference;
import com.isu.service.onboarding.configuration.OnboardConfig;
import com.isu.service.onboarding.configuration.Validate;
import com.isu.service.onboarding.ekyc.EkycRequestActivity;

import org.json.JSONException;
import org.json.JSONObject;

public class FetchDigiActivity extends AppCompatActivity {
    private static final String TAG = FetchDigiActivity.class.getSimpleName();
    OnBoardSharedPreference preference;
    Button refetch;
    TextView fetchText;
    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        showDigi();

        setContentView(R.layout.activity_fetch_digi);
        OnboardConfig.whiteStatusNav(this);

        preference = new OnBoardSharedPreference(this);

        refetch = findViewById(R.id.fetch_reverify);
        fetchText = findViewById(R.id.fetch_text);

        refetch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fetchPan();
            }
        });


    }

    public void showDigi() {
        boolean installed = appInstalledOrNot("com.digilocker.android");
        try {
            AlertDialog.Builder alertbuilderupdate;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                alertbuilderupdate = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
            } else {
                alertbuilderupdate = new AlertDialog.Builder(this);
            }
            alertbuilderupdate.setCancelable(false);
            // String message = "Session is already running !!! Please login after sometimes.";
            alertbuilderupdate.setTitle("Alert")
                    .setMessage("Please add PAN card to your Digi-Locker. After adding it continue your process from here.")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            try {
                                if (installed) {
                                    PackageManager manager = getPackageManager();
                                    Intent sIntent = manager.getLaunchIntentForPackage("com.digilocker.android");

                                    sIntent.addCategory(Intent.CATEGORY_LAUNCHER);
                                    startActivity(sIntent);


                                } else {
//                                    String url = "https://accounts.digitallocker.gov.in/signin";
//                                    Intent i = new Intent(Intent.ACTION_VIEW);
//                                    i.setData(Uri.parse(url));
//                                    startActivity(i);
                                    startActivity(new Intent(FetchDigiActivity.this, DigiWebActivity.class));
                                }
                            } catch (Exception e) {

                            }
                            dialog.cancel();
                        }
                    });
//                    .show();
            AlertDialog alert11 = alertbuilderupdate.create();
            alert11.show();
        } catch (Exception e) {

        }
    }

    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }
        return false;
    }

    private void fetchPan() {

        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();

        String name = preference.getStringValue(OnboardConfig.USER_NAME_KEY);
        String url = OnboardConfig.getDigiRefetchUrl() + "code=" + name + "&state=" + name + "&caller=" + name;

        AndroidNetworking.get(url)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e(TAG, "onResponse: " + response);
                        try {
                            String status = response.getString("status");
                            if (status.equals("0")) {
                                String pan = response.getString("PAN");
//                                preference.setStringValue(OnboardConfig.PAN_CARD_KEY, pan);
                                startActivity(new Intent(FetchDigiActivity.this, EkycRequestActivity.class));
                                dialog.dismiss();
                                finish();

                            } else {
                                Validate.showAlert(FetchDigiActivity.this, response.getString("statusDesc"));
                                fetchText.setText(response.getString("statusDesc"));
                                dialog.dismiss();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            dialog.dismiss();
                            fetchText.setText("Error in getting PAN card. Please try later");
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        try {
                            JSONObject errorObject = new JSONObject(anError.getErrorBody());
                            Log.e(TAG, "onError: " + errorObject);
                            Toast.makeText(FetchDigiActivity.this, errorObject.getString("statusDesc"), Toast.LENGTH_LONG).show();
                            fetchText.setText(errorObject.getString("statusDesc"));
                            dialog.dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            dialog.dismiss();
                            fetchText.setText("Error in getting PAN card. Please try later");
                        }

                    }
                });
    }
    @Override
    public void onBackPressed() {
        Validate.showExitAlert(this);
    }
}
