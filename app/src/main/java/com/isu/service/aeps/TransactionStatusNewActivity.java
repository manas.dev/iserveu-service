package com.isu.service.aeps;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.isu.service.R;
import com.isu.service.aeps.utils.PermissionsChecker;
import com.isu.service.matm2.Utils.SdkConstants;
import com.paxsz.easylink.api.EasyLinkSdkManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TransactionStatusNewActivity extends AppCompatActivity {

    private LinearLayout ll_maiin;
    LinearLayout detailsLayout;
    private TextView statusMsgTxt, statusDescTxt;
    private ImageView status_icon;
    private TextView date_time, rref_num, aadhar_number, bank_name, card_amount, card_transaction_type, card_transaction_amount, txnID;
    public EasyLinkSdkManager manager;
    private Button backBtn,downloadBtn,printBtn;
    LinearLayout ll13, ll12;
    String balance = "N/A";
    String amount = "N/A";
    String referenceNo = "N/A";
    String bankName = "N/A";
    String aadharCard = "N/A";
    String txnid = "N/A";
    PermissionsChecker checker;
    Context mContext;
    BluetoothAdapter B;
    private int STORAGE_PERMISSION_CODE=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (SdkConstants.aepsStatusLayout == 0) {
            setContentView(R.layout.activity_transaction_status_aeps2);
        } else {
            setContentView(SdkConstants.aepsStatusLayout);
        }
        //Runtime permission request required if Android permission >= Marshmallow
        checker = new PermissionsChecker(this);
        mContext = getApplicationContext();

        ll_maiin = findViewById(R.id.ll_maiin);
        ll13 = findViewById(R.id.ll13);
        ll12 = findViewById(R.id.ll12);
        detailsLayout = findViewById(R.id.detailsLayout);
        B= BluetoothAdapter.getDefaultAdapter();

        txnID = findViewById(R.id.txnID);
        card_transaction_amount = findViewById(R.id.card_transaction_amount);
        statusMsgTxt = findViewById(R.id.statusMsgTxt);
        status_icon = findViewById(R.id.status_icon);
        aadhar_number = findViewById(R.id.aadhar_number);
        rref_num = findViewById(R.id.rref_num);
        bank_name = findViewById(R.id.bank_name);
        card_transaction_type = findViewById(R.id.card_transaction_type);
        card_amount = findViewById(R.id.card_amount);
        statusDescTxt = findViewById(R.id.statusDescTxt);
        backBtn = findViewById(R.id.backBtn);
        downloadBtn = findViewById(R.id.downloadBtn);
        printBtn = findViewById(R.id.printBtn);
        date_time = findViewById(R.id.date_time);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd : HH.mm.ss");
        String currentDateandTime = sdf.format(new Date());
        date_time.setText(currentDateandTime);
        if (getIntent().getSerializableExtra(SdkConstants.TRANSACTION_STATUS_KEY) == null) {
            ll_maiin.setBackgroundColor(getResources().getColor(R.color.statusfail));
            status_icon.setImageResource(R.drawable.ic_errorrr);
            statusMsgTxt.setText("Failed.");
            detailsLayout.setVisibility(View.GONE);
            backBtn.setBackgroundResource(R.drawable.button_background_fail);
            downloadBtn.setBackgroundResource(R.drawable.button_background_fail);
            printBtn.setBackgroundResource(R.drawable.button_background_fail);

        } else {
            TransactionStatusModel transactionStatusModel = (TransactionStatusModel) getIntent().getSerializableExtra(SdkConstants.TRANSACTION_STATUS_KEY);

            if (transactionStatusModel.getStatus().trim().equalsIgnoreCase("0")) {

                aadharCard = transactionStatusModel.getAadharCard();
                if (transactionStatusModel.getAadharCard() == null) {
                    aadharCard = "N/A";
                } else {
                    if (transactionStatusModel.getAadharCard().equalsIgnoreCase("")) {
                        aadharCard = "N/A";
                    } else {
                        StringBuffer buf = new StringBuffer(aadharCard);
                        buf.replace(0, 10, "XXXX-XXXX-");
                        System.out.println(buf.length());
                        aadharCard = buf.toString();
                    }
                }


                if (transactionStatusModel.getTxnID() != null && !transactionStatusModel.getTxnID().matches("")) {
                    txnid = transactionStatusModel.getTxnID();
                }
                if (transactionStatusModel.getBankName() != null && !transactionStatusModel.getBankName().matches("")) {
                    bankName = transactionStatusModel.getBankName();
                }
                if (transactionStatusModel.getReferenceNo() != null && !transactionStatusModel.getReferenceNo().matches("")) {
                    referenceNo = transactionStatusModel.getReferenceNo();
                }

                if (transactionStatusModel.getBalanceAmount() != null && !transactionStatusModel.getBalanceAmount().matches("")) {
                    balance = transactionStatusModel.getBalanceAmount();
                    if (balance.contains(":")) {
                        String[] separated = balance.split(":");
                        balance = separated[1].trim();
                    }
                }

                if (transactionStatusModel.getTransactionAmount() != null && !transactionStatusModel.getTransactionAmount().matches("")) {
                    amount = transactionStatusModel.getTransactionAmount();
                }


                if (transactionStatusModel.getTransactionType().equalsIgnoreCase("Cash Withdrawal")) {
                    ll13.setVisibility(View.VISIBLE);
                    txnID.setText(txnid);
                    rref_num.setText(referenceNo);
                    aadhar_number.setText(aadharCard);
                    bank_name.setText(bankName);
                    card_amount.setText(balance);
                    card_transaction_amount.setText(amount);
                    card_transaction_type.setText(transactionStatusModel.getTransactionType());

                } else if (transactionStatusModel.getTransactionType().equalsIgnoreCase("Balance Enquery")||transactionStatusModel.getTransactionType().equalsIgnoreCase("Balance Enquiry")) {
                    ll13.setVisibility(View.VISIBLE);
                    txnID.setText(txnid);
                    rref_num.setText(referenceNo);
                    aadhar_number.setText(aadharCard);
                    bank_name.setText(bankName);
                    card_amount.setText(balance);
                    card_transaction_amount.setText(amount);
                    card_transaction_type.setText("Balance Enquiry");

                }
            } else {
                String aadharCard = transactionStatusModel.getAadharCard();
                if (transactionStatusModel.getAadharCard() == null) {
                    aadharCard = "N/A";
                } else {
                    if (transactionStatusModel.getAadharCard().equalsIgnoreCase("")) {
                        aadharCard = "N/A";
                    } else {
                        StringBuffer buf = new StringBuffer(aadharCard);
                        buf.replace(0, 10, "XXXX-XXXX-");
                        System.out.println(buf.length());
                        aadharCard = buf.toString();
                    }
                }
                if (transactionStatusModel.getTxnID() != null && !transactionStatusModel.getTxnID().matches("")) {
                    txnid = transactionStatusModel.getTxnID();
                }
                if (transactionStatusModel.getBankName() != null && !transactionStatusModel.getBankName().matches("")) {
                    bankName = transactionStatusModel.getBankName();
                }

                if (transactionStatusModel.getReferenceNo() != null && !transactionStatusModel.getReferenceNo().matches("")) {
                    referenceNo = transactionStatusModel.getReferenceNo();
                }

                if (transactionStatusModel.getBalanceAmount() != null && !transactionStatusModel.getBalanceAmount().matches("")) {
                    balance = transactionStatusModel.getBalanceAmount();
                    if (balance.contains(":")) {
                        String[] separated = balance.split(":");
                        balance = separated[1].trim();
                    }
                }

                if (transactionStatusModel.getTransactionAmount() != null && !transactionStatusModel.getTransactionAmount().matches("")) {
                    amount = transactionStatusModel.getTransactionAmount();
                }
                ll_maiin.setBackgroundColor(Color.parseColor("#D94237"));
                status_icon.setImageResource(R.drawable.ic_errorrr);
                backBtn.setBackgroundResource(R.drawable.button_background_fail);
                downloadBtn.setBackgroundResource(R.drawable.button_background_fail);
                printBtn.setBackgroundResource(R.drawable.button_background_fail);
                statusMsgTxt.setText("Failed");
                statusDescTxt.setText(transactionStatusModel.getApiComment());

                if (transactionStatusModel.getTransactionType().equalsIgnoreCase("Cash Withdrawal")) {
                    ll13.setVisibility(View.VISIBLE);
                    txnID.setText(txnid);
                    rref_num.setText(referenceNo);
                    aadhar_number.setText(aadharCard);
                    bank_name.setText(bankName);
                    card_amount.setText(balance);
                    card_transaction_amount.setText(amount);
                    card_transaction_type.setText(transactionStatusModel.getTransactionType());

                } else if (transactionStatusModel.getTransactionType().equalsIgnoreCase("Balance Enquery") ||transactionStatusModel.getTransactionType().equalsIgnoreCase("Balance Enquiry")) {
                    ll13.setVisibility(View.VISIBLE);
                    txnID.setText(txnid);
                    rref_num.setText(referenceNo);
                    aadhar_number.setText(aadharCard);
                    bank_name.setText(bankName);
                    card_amount.setText(balance);
                    card_transaction_amount.setText(amount);
                    card_transaction_type.setText("Balance Enquiry");

                }
            }
        }
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


            }


    private void showBrandSetAlert(){
        try{
            AlertDialog.Builder builder1 = new AlertDialog.Builder(TransactionStatusNewActivity.this);
            builder1.setMessage("Unable to download/print the receipt. Please contact admin.");
            builder1.setTitle("Warning!!!");
            builder1.setCancelable(false);
            builder1.setPositiveButton(
                    "GOT IT",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

            AlertDialog alert11 = builder1.create();
            alert11.show();
        }catch (Exception e){

        }
    }


}