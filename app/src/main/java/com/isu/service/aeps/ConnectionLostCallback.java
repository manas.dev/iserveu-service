package com.isu.service.aeps;

public interface ConnectionLostCallback {
    public void connectionLost();
}
