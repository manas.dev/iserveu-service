package com.isu.service;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.hardware.usb.UsbDevice;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.isu.service.aeps.AEPS2HomeActivity;
import com.isu.service.aeps.BioAuthActivity;
import com.isu.service.aeps.CoreAEPSHomeActivity;
import com.isu.service.matm2.BluetoothActivity;
import com.isu.service.matm2.Constants;
import com.isu.service.matm2.PosActivity;
import com.isu.service.matm2.Utils.SdkConstants;
import com.isu.service.onboarding.UserTypeActivity;
import com.isu.service.onboarding.configuration.OnboardConfig;

import static com.isu.service.matm2.Utils.SdkConstants.REQUEST_CODE;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private UsbDevice usbDevice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        checkLocation();

        Bundle b = getIntent().getExtras();

        if (b != null) {
            Log.e(TAG, "onCreate: " + b);
            if (b.containsKey("ActivityName")) {
                String activityName = b.getString("ActivityName");
                Log.e(TAG, "onCreate: " + activityName);
                if (activityName.equals("SelfOnBoard")) {
                    OnboardConfig.userAdmin = b.getString("AdminName");
                    OnboardConfig.createBy = b.getString("CreatedBy");
                    Intent i = new Intent(MainActivity.this, UserTypeActivity.class);
                    startActivity(i);
                    finish();
                }
                else if (activityName.equals("Bluetooth")) {
                    Constants.applicationType = b.getString("ApplicationType");
                    if (Constants.applicationType.equals("CORE")) {
                        Constants.token = b.getString("UserToken");
                        Constants.user_id = b.getString("UserName");
                    } else {
                        Constants.loginID = b.getString("LoginID");
                        Constants.encryptedData = b.getString("EncryptedData");
                    }
                    Intent intent = new Intent(MainActivity.this, BluetoothActivity.class);
                    startActivity(intent);
                    finish();
                }
                else if (activityName.equals("mATM2")) {
                    if (PosActivity.isBlueToothConnected(this)) {
                        Constants.applicationType = b.getString("ApplicationType");
                        if (Constants.applicationType.equals("CORE")) {
                            Constants.token = b.getString("UserToken");
                            Constants.user_id = b.getString("UserName");
                            Constants.tokenFromCoreApp = b.getString("UserToken");
                            Constants.userNameFromCoreApp = b.getString("UserName");
                        } else {
                            Constants.loginID = b.getString("LoginID");
                            Constants.encryptedData = b.getString("EncryptedData");
                        }

                        Constants.paramA = b.getString("ParamA");
                        Constants.paramB = b.getString("ParamB");
                        Constants.paramC = b.getString("ParamC");

                        Constants.transactionType = b.getString("TransactionType");
                        Constants.transactionAmount = b.getString("Amount");

                        Intent intent = new Intent(MainActivity.this, PosActivity.class);
                        startActivityForResult(intent, REQUEST_CODE);
                    } else {
                        showAlertDialog("Please pair the bluetooth device");
                    }
                }
                else if (activityName.equals("CoreAeps")) {
                    SdkConstants.applicationType = b.getString("ApplicationType");
                    SdkConstants.transactionType = b.getString("TransactionType");
                    SdkConstants.transactionAmount = b.getString("Amount");

                    if (SdkConstants.applicationType.equals("CORE")) {
                        SdkConstants.userNameFromCoreApp = b.getString("UserName");
                        SdkConstants.tokenFromCoreApp = b.getString("UserToken");
                        SdkConstants.MANUFACTURE_FLAG = b.getString("ManufactureFlag");
                    } else {
                        SdkConstants.loginID = b.getString("LoginID");
                        SdkConstants.encryptedData = b.getString("EncryptedData");
                    }

                    SdkConstants.paramA = b.getString("ParamA");
                    SdkConstants.paramB = b.getString("ParamB");
                    SdkConstants.paramC = b.getString("ParamC");

                    Intent intent = new Intent(MainActivity.this, CoreAEPSHomeActivity.class);
                    startActivityForResult(intent, REQUEST_CODE);
                }
                else if (activityName.equals("AEPS2")) {

                    SdkConstants.applicationType = b.getString("ApplicationType");
                    SdkConstants.transactionType = b.getString("TransactionType");
                    SdkConstants.transactionAmount = b.getString("Amount");

                    if (SdkConstants.applicationType.equals("CORE")) {
                        SdkConstants.userNameFromCoreApp = b.getString("UserName");
                        SdkConstants.tokenFromCoreApp = b.getString("UserToken");
                        SdkConstants.MANUFACTURE_FLAG = b.getString("ManufactureFlag");
                    } else {
                        SdkConstants.loginID = b.getString("LoginID");
                        SdkConstants.encryptedData = b.getString("EncryptedData");
                    }

                    SdkConstants.paramA = b.getString("ParamA");
                    SdkConstants.paramB = b.getString("ParamB");
                    SdkConstants.paramC = b.getString("ParamC");

                    Intent intent = new Intent(MainActivity.this, AEPS2HomeActivity.class);
                    startActivityForResult(intent, REQUEST_CODE);

                }
                else if (activityName.equals("BioAuth")) {

                    if (SdkConstants.applicationType.equals("CORE")) {
                        SdkConstants.applicationUserName = b.getString("AppUserName");
                        SdkConstants.userNameFromCoreApp = b.getString("UserName");
                        SdkConstants.tokenFromCoreApp = b.getString("UserToken");
                        SdkConstants.MANUFACTURE_FLAG = b.getString("ManufactureFlag");
                    } else {
                        SdkConstants.loginID = b.getString("LoginID");
                        SdkConstants.encryptedData = b.getString("EncryptedData");
                    }

                    SdkConstants.paramA = b.getString("ParamA");
                    SdkConstants.paramB = b.getString("ParamB");
                    SdkConstants.paramC = b.getString("ParamC");

                    Intent intent = new Intent(MainActivity.this, BioAuthActivity.class);
                    startActivityForResult(intent, REQUEST_CODE);

                }
                else if (activityName.equals("CompleteOB")) {
                    finish();
                }
            } else {
                showAlertDialog("Invalid transaction data, Please use this app with parent application");
            }

        } else {
            showAlertDialog("Invalid transaction data, Please use this app with parent application");
        }

    }

    public void showAlertDialog(final String msg) {
        if (!isFinishing()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    AlertDialog.Builder alertbuilderupdate;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        alertbuilderupdate = new AlertDialog.Builder(MainActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
                    } else {
                        alertbuilderupdate = new AlertDialog.Builder(MainActivity.this);
                    }
                    alertbuilderupdate.setCancelable(false);
                    // String message = "A new version of "+ getResources().getString(R.string.app_name)+" is available. Please update to version "+latestVersion +" "+ getResources().getString(R.string.youAreNotUpdatedMessage1);
                    alertbuilderupdate.setTitle("Alert")
                            .setMessage(msg)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                    finishAffinity();
                                    System.exit(0);
                                }
                            })
                            .show();
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null && resultCode == RESULT_OK) {
            Log.e(TAG, "onActivityResult: data "+data.getStringExtra("TRANS_TYPE"));
            Log.e(TAG, "onActivityResult: data "+resultCode );
            setResult(RESULT_OK, data);
            finish();


        } else {
            Log.e(TAG, "onActivityResult: no data found, requestCode " + requestCode + " resultCode " + resultCode);
            finish();
        }
    }

    public void checkLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_LOCATION);

        } else {
            // No explanation needed, we can request the permission.
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_LOCATION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST_LOCATION) {
            // If request is cancelled, the result arrays are empty.
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                // permission was granted, yay! Do the
                // location-related task you need to do.
                if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {

                    //Request location updates:
                    forceUpdate();

                }

            } else {

                forceUpdate();
                // permission denied, boo! Disable the
                // functionality that depends on this permission.

            }
            return;
        }

    }

    public void forceUpdate() {
        PackageManager packageManager = this.getPackageManager();
        PackageInfo packageInfo = null;

        try {
            packageInfo = packageManager.getPackageInfo(getPackageName(), 0);

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        String currentVersion = packageInfo.versionName;

        new ForceUpdateAsync(currentVersion, MainActivity.this, new ForceUpdateAsync.AsyncListener() {
            @Override
            public void doStuff(String latestVersion, String currentVersion) {
                if (latestVersion != null) {
                    if (!currentVersion.equalsIgnoreCase(latestVersion)) {
                        showForceUpdateDialog(latestVersion);
                    }
                }
            }
        }).execute();
    }

    public void showForceUpdateDialog(final String latestVersion) {

        if (!isFinishing()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    AlertDialog.Builder alertbuilderupdate;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        alertbuilderupdate = new AlertDialog.Builder(MainActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
                    } else {
                        alertbuilderupdate = new AlertDialog.Builder(MainActivity.this);
                    }
                    alertbuilderupdate.setCancelable(false);
                    // String message = "A new version of "+ getResources().getString(R.string.app_name)+" is available. Please update to version "+latestVersion +" "+ getResources().getString(R.string.youAreNotUpdatedMessage1);
                    String message = "A new version of this app is available. Please update to version " + latestVersion + " now";
                    alertbuilderupdate.setTitle("Update Available")
                            .setMessage(message)
                            .setPositiveButton("Update", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                    Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=com.linkmatm.service&hl=en_US");

                                    startActivity(new Intent(Intent.ACTION_VIEW, uri));
                                    dialog.dismiss();
                                }
                            })
                            .show();
                }
            });
        }

    }

}